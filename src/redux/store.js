/* eslint-disable no-use-before-define */

import dialogsReducer from './dialogsReducer';
import profileReducer from './profileReducer';
import sidebarReducer from './sidebarReducer';

const store = {
  _state: {
    profilePage: {
      posts: [
        { id: 1, text: 'Hi, how are you?', likesCount: 20 },
        { id: 2, text: "it's my first post", likesCount: 10 },
      ],
      newPostText: '',
    },

    messagesPage: {
      messages: [
        { id: 1, text: 'Hi' },
        { id: 2, text: 'How is your IT-kamasutra' },
        { id: 3, text: 'Yo' },
        { id: 4, text: 'Yo' },
        { id: 5, text: 'Yo' },
      ],
      dialogs: [
        { id: 1, name: 'Dimych' },
        { id: 2, name: 'Andrey' },
        { id: 3, name: 'Sveta' },
        { id: 4, name: 'Sasha' },
        { id: 5, name: 'Victor' },
        { id: 6, name: 'Valera' },
      ],
      newMessageText: '',
    },

    sidebar: {
      userId: 1,
      friends: [
        { id: 1, name: 'Dimych' },
        { id: 2, name: 'Andrey' },
        { id: 3, name: 'Sveta' },
      ],
    },
  },

  getState() {
    return this._state;
  },

  _subscriber() {
    // eslint-disable-next-line no-console
    console.log('no subscribers');
  },

  subscribe(observer) {
    this._subscriber = observer;
  },

  dispatch(action) {
    this._state.profilePage = profileReducer(this._state.profilePage, action);
    this._state.messagesPage = dialogsReducer(this._state.messagesPage, action);
    this._state.sidebarPage = sidebarReducer(this._state.sidebarPage);

    this._subscriber();
  },
};

export default store;
