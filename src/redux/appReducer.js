import { authUser } from './authReducer';

const INITIALIZED_SUCCESS = 'social/app/INITIALIZED_SUCCESS';

const initialState = {
  appInitialize: false,
};

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case INITIALIZED_SUCCESS: {
      return {
        ...state,
        appInitialize: true,
      };
    }

    default:
      return state;
  }
};

export const initializedSuccess = () => ({
  type: INITIALIZED_SUCCESS,
});

export const initializingApp = () => (dispatch) => {
  dispatch(authUser()).then(() => {
    dispatch(initializedSuccess());
  });
};

export default appReducer;
