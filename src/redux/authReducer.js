/* eslint-disable indent */
/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable operator-linebreak */
import { authAPI, profileAPI } from 'api/api';

const SET_USER_DATA = 'social/auth/SET_USER_DATA';
const SET_SERVER_ERROR = 'social/auth/SET_SERVER_ERROR';

const initialState = {
  userId: null,
  email: null,
  login: null,
  isAuth: false,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_DATA: {
      return {
        ...state,
        ...action.payload,
      };
    }

    default:
      return state;
  }
};

export const setUserData = (userId, email, login, isAuth) => ({
  type: SET_USER_DATA,
  payload: { userId, email, login, isAuth },
});

export const setServerError = (error) => ({
  type: SET_SERVER_ERROR,
  error,
});

export const authUser = () => (dispatch) => {
  return authAPI.userAuth().then((data) => {
    if (data.resultCode === 0) {
      const { id, email, login } = data.data;
      dispatch(setUserData(id, email, login, true));

      profileAPI.getUserProfile(id).then((user) => {
        return user.photos.small;
      });
    }
  });
};

export const login =
  (email, password, rememberMe = false) =>
  (dispatch) => {
    return authAPI.login(email, password, rememberMe).then((data) => {
      if (data.resultCode === 0) {
        dispatch(authUser());
      } else {
        throw new Error(data.messages[0]);
      }
    });
  };

export const logout = () => (dispatch) => {
  authAPI.logout().then((data) => {
    if (data.resultCode === 0) {
      dispatch(setUserData(null, null, null, false));
    }
  });
};

export default authReducer;
