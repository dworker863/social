import { profileAPI } from 'api/api';

const ADD_POST = 'social/profile/ADD_POST';
const SET_USER_PROFILE = 'social/profile/SET_USER_PROFILE';
const SET_PROFILE_STATUS = 'social/profile/SET_PROFILE_STATUS';

const initialState = {
  posts: [
    { id: 1, text: 'Hi, how are you?', likesCount: 20 },
    { id: 2, text: "it's my first post", likesCount: 10 },
  ],
  profile: null,
  status: '',
};

const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_POST: {
      return {
        ...state,
        posts: [
          ...state.posts,
          {
            id: 5,
            text: action.newPostText,
            likesCount: 0,
          },
        ],
      };
    }

    case SET_USER_PROFILE: {
      return { ...state, profile: action.userProfile };
    }

    case SET_PROFILE_STATUS: {
      return { ...state, status: action.status };
    }
    default:
      return state;
  }
};

export const addPost = (newPostText) => ({ type: ADD_POST, newPostText });

export const setUserProfile = (userProfile) => ({
  type: SET_USER_PROFILE,
  userProfile,
});

export const setProfileStatus = (status) => ({
  type: SET_PROFILE_STATUS,
  status,
});

export const getUserProfile = (userId) => (dispatch) => {
  if (!userId) {
    userId = 15323;
  }
  profileAPI.getUserProfile(userId).then((data) => {
    dispatch(setUserProfile(data));
  });
};

export const getProfileStatus = (userId) => (dispatch) => {
  if (!userId) {
    userId = 15323;
  }
  profileAPI.getProfileStatus(userId).then((data) => {
    dispatch(setProfileStatus(data));
  });
};

export const updateProfileStatus = (status) => (dispatch) => {
  profileAPI.updateProfileStatus(status).then((response) => {
    if (response.resultCode === 0) {
      dispatch(setProfileStatus(status));
    }
  });
};

export default profileReducer;
