const initialState = {
  userId: 1,
  friends: [
    { id: 1, name: 'Dimych' },
    { id: 2, name: 'Andrey' },
    { id: 3, name: 'Sveta' },
  ],
};

const sidebarReducer = (state = initialState) => {
  return state;
};

export default sidebarReducer;
