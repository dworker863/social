const ADD_MESSAGE = 'social/dialogs/ADD-MESSAGE';

const initialState = {
  messages: [
    { id: 1, text: 'Hi' },
    { id: 2, text: 'How is your IT-kamasutra' },
    { id: 3, text: 'Yo' },
    { id: 4, text: 'Yo' },
    { id: 5, text: 'Yo' },
  ],
  dialogs: [
    { id: 1, name: 'Dimych' },
    { id: 2, name: 'Andrey' },
    { id: 3, name: 'Sveta' },
    { id: 4, name: 'Sasha' },
    { id: 5, name: 'Victor' },
    { id: 6, name: 'Valera' },
  ],
};

const dialogsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_MESSAGE: {
      return {
        ...state,
        messages: [...state.messages, { id: 6, text: action.newMessage }],
      };
    }

    default:
      return state;
  }
};

export const addMessage = (newMessage) => ({
  type: ADD_MESSAGE,
  newMessage,
});

export default dialogsReducer;
