import axios from 'axios';

const instance = axios.create({
  withCredentials: true,
  baseURL: 'https://social-network.samuraijs.com/api/1.0/',
  headers: {
    'API-KEY': '732421ff-7141-4237-84b2-fee186180ae8',
  },
});

export const usersAPI = {
  getUsers(currentPage = 1, usersCount = 10) {
    return instance
      .get(`users?page=${currentPage}&count=${usersCount}`)
      .then((response) => {
        return response.data;
      });
  },

  follow(userId) {
    return instance
      .post(`https://social-network.samuraijs.com/api/1.0/follow/${userId}`)
      .then((response) => {
        return response.data;
      });
  },

  unfollow(userId) {
    return instance
      .delete(`https://social-network.samuraijs.com/api/1.0/follow/${userId}`)
      .then((response) => {
        return response.data;
      });
  },
};

export const profileAPI = {
  getUserProfile(userId) {
    return instance.get(`profile/${userId}`).then((response) => {
      return response.data;
    });
  },

  getProfileStatus(userId) {
    return instance.get(`profile/status/${userId}`).then((response) => {
      return response.data;
    });
  },

  updateProfileStatus(status) {
    return instance.put('profile/status', { status }).then((response) => {
      return response.data;
    });
  },
};

export const authAPI = {
  userAuth() {
    return instance.get('auth/me').then((response) => {
      return response.data;
    });
  },

  login(email, password, rememberMe) {
    return instance
      .post('auth/login', { email, password, rememberMe })
      .then((response) => {
        return response.data;
      });
  },

  logout() {
    return instance.delete('auth/login').then((response) => {
      return response.data;
    });
  },
};
