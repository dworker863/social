import * as Yup from 'yup';

export const yupMessage = () => Yup.mixed().required('Required');
export const yupLogin = () =>
  // eslint-disable-next-line implicit-arrow-linebreak
  Yup.string().email('Invalid email address').required('Required');
export const yupPassword = () => Yup.mixed().required('Required');
