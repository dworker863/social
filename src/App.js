import React from 'react';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { initializingApp } from 'redux/appReducer';
import './App.css';
import DialogsContainer from 'components/Dialogs/DialogsContainer';
import UsersContainer from 'components/Users/UsersContainer';
import HeaderContainer from 'components/Header/HeaderContainer';
import LoginContainer from 'components/Login/LoginContainer';
import Preloader from 'components/common/Preloader/Preloader';
import Music from './components/Music/Music';
import Navbar from './components/Sidebar/Sidebar';
import News from './components/News/News';
import Settings from './components/Settings/Settings';
import ProfileContainer from './components/Profile/ProfileContainer';

// eslint-disable-next-line react/prefer-stateless-function
class App extends React.Component {
  componentDidMount() {
    const { appInitializing } = this.props;
    appInitializing();
  }

  render() {
    const { store, appInitialize } = this.props;

    if (!appInitialize) return <Preloader />;

    return (
      <div className="app-wrapper">
        <HeaderContainer />
        <Navbar sidebar={store.getState().sidebar} />
        <main className="content">
          <Route path="/profile/:userId?" render={() => <ProfileContainer />} />
          <Route path="/dialogs" render={() => <DialogsContainer />} />
          <Route path="/news" component={News} />
          <Route path="/music" component={Music} />
          <Route path="/settings" component={Settings} />
          <Route path="/users" render={() => <UsersContainer />} />
          <Route path="/login" render={() => <LoginContainer />} />
        </main>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  appInitialize: state.app.appInitialize,
});

export default connect(mapStateToProps, { appInitializing: initializingApp })(
  App,
);
