import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

const withAuthRedirect = (Component) => {
  const RedirectComponent = ({ isAuth }) => {
    if (!isAuth) return <Redirect to="/login" />;
    return <Component />;
  };

  const mapStateToProps = (state) => ({
    isAuth: state.auth.isAuth,
  });

  return connect(mapStateToProps)(RedirectComponent);
};

export default withAuthRedirect;
