import Error from 'components/common/Error/Error';
import { useFormik } from 'formik';
import { NavLink } from 'react-router-dom';
import { yupMessage } from 'utils/validate';
import * as Yup from 'yup';
import classes from './Dialogs.module.css';

const MessageForm = ({ addMessage }) => {
  const formik = useFormik({
    initialValues: {
      newMessage: '',
    },
    validationSchema: Yup.object({
      newMessage: yupMessage(),
    }),
    onSubmit(values, { resetForm }) {
      addMessage(values.newMessage);
      resetForm();
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <div>
        <textarea
          name="newMessage"
          id="newMessage"
          placeholder="New message"
          {...formik.getFieldProps('newMessage')}
        />
        {formik.touched.newMessage && formik.errors.newMessage ? (
          <Error error={formik.errors.newMessage} />
        ) : null}
      </div>
      <div>
        <button type="submit">Add message</button>
      </div>
    </form>
  );
};

const DialogItem = ({ name, id }) => {
  return (
    <div className={`${classes.dialog} ${classes.active}`}>
      <img
        className={classes.avatar}
        src="https://image.freepik.com/free-vector/businessman-character-avatar-isolated_24877-60111.jpg"
        alt="avatar"
        width="30px"
      />
      <NavLink to={`/dialogs/${id}`}>{name}</NavLink>
    </div>
  );
};

const Message = ({ message }) => {
  return <div className={classes.message}>{message}</div>;
};

const Dialogs = ({ state, addMessage }) => {
  return (
    <div>
      <div className={classes.dialogs}>
        <div className={classes.dialogsItems}>
          {state.dialogs.map((dialog) => (
            <DialogItem name={dialog.name} id={dialog.id} key={dialog.id} />
          ))}
        </div>
        <div className={classes.messages}>
          {state.messages.map((message) => (
            <Message message={message.text} key={message.id} />
          ))}
        </div>
        <MessageForm addMessage={addMessage} />
      </div>
    </div>
  );
};

export default Dialogs;
