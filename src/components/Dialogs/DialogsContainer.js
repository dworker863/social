import withAuthRedirect from 'hoc/AuthRedirect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { addMessage } from 'redux/dialogsReducer';
import Dialogs from './Dialogs';

const mapStateToProps = (state) => {
  return {
    state: state.messagesPage,
    isAuth: state.auth.isAuth,
  };
};

const DialogsContainer = compose(
  withAuthRedirect,
  connect(mapStateToProps, { addMessage }),
)(Dialogs);

export default DialogsContainer;
