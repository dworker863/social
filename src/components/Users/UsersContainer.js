import { connect } from 'react-redux';
import {
  setCurrentPage,
  setFollow,
  getUsers,
  setUnfollow,
} from 'redux/usersReducer';
import UsersContainerAPI from './UsersContainerAPI';

const mapStateToProps = (state) => {
  return {
    users: state.usersPage.users,
    usersCount: state.usersPage.usersCount,
    currentPage: state.usersPage.currentPage,
    totalUsersCount: state.usersPage.totalUsersCount,
    isFetching: state.usersPage.isFetching,
    followingInProgress: state.usersPage.followingInProgress,
  };
};

export default connect(mapStateToProps, {
  setCurrentPage,
  setFollow,
  getUsers,
  setUnfollow,
})(UsersContainerAPI);
