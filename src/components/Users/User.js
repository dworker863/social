/* eslint-disable react/destructuring-assignment */
import userPhotoDefault from 'assets/userPhotoDefault.png';
import { NavLink } from 'react-router-dom';
import classes from './Users.module.css';

const User = ({ user, setFollow, setUnfollow, followingInProgress }) => {
  return (
    <div>
      <span>
        <div>
          <NavLink to={`/profile/${user.id}`}>
            <img
              className={classes.photo}
              src={user.photos.small ? user.photos.small : userPhotoDefault}
              alt={user.name}
            />
          </NavLink>
        </div>
        <div>
          {user.followed ? (
            <button
              type="button"
              disabled={followingInProgress.some((id) => {
                return id === user.id;
              })}
              onClick={() => {
                setUnfollow(user.id);
              }}
            >
              Unfollow
            </button>
          ) : (
            <button
              type="button"
              disabled={followingInProgress.some((id) => id === user.id)}
              onClick={() => {
                setFollow(user.id);
              }}
            >
              Follow
            </button>
          )}
        </div>
      </span>
      <span>
        <span>
          <div>{user.name}</div>
          <div>{user.status}</div>
        </span>
        <span>
          <div>user.location.city</div>
          <div>user.location.country</div>
        </span>
      </span>
    </div>
  );
};

export default User;
