import React from 'react';
import Preloader from 'components/common/Preloader/Preloader';

import Users from './Users';

class UsersContainerAPI extends React.Component {
  constructor(props) {
    super(props);
    this.onPageChange = this.onPageChange.bind(this);
  }

  componentDidMount() {
    const { getUsers, currentPage, usersCount } = this.props;
    getUsers(currentPage, usersCount);
  }

  onPageChange(page) {
    const { getUsers, usersCount } = this.props;
    getUsers(page, usersCount);
  }

  render() {
    const {
      users,
      usersCount,
      currentPage,
      totalUsersCount,
      isFetching,
      followingInProgress,
      setFollow,
      setUnfollow,
    } = this.props;

    return (
      <>
        {isFetching ? <Preloader /> : null}
        <Users
          users={users}
          currentPage={currentPage}
          usersCount={usersCount}
          totalUsersCount={totalUsersCount}
          onPageChange={this.onPageChange}
          followingInProgress={followingInProgress}
          setFollow={setFollow}
          setUnfollow={setUnfollow}
        />
      </>
    );
  }
}

export default UsersContainerAPI;
