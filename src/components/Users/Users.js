/* eslint-disable react/destructuring-assignment */
import Pagination from 'components/common/Pagination/Pagination';
import User from './User';

const Users = ({
  users,
  setFollow,
  setUnfollow,
  usersCount,
  currentPage,
  totalUsersCount,
  onPageChange,
  followingInProgress,
}) => {
  return (
    <div>
      <Pagination
        totalItemsCount={totalUsersCount}
        itemsCount={usersCount}
        currentPage={currentPage}
        onPageChange={onPageChange}
      />
      {users.map((user) => (
        <User
          key={user.id}
          user={user}
          setFollow={setFollow}
          setUnfollow={setUnfollow}
          followingInProgress={followingInProgress}
        />
      ))}
    </div>
  );
};

export default Users;
