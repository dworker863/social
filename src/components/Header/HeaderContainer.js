import { connect } from 'react-redux';
import { authUser, logout } from 'redux/authReducer';
import HeaderContainerAPI from './HeaderContainerAPI';

const mapStateToProps = (state) => ({
  login: state.auth.login,
  isAuth: state.auth.isAuth,
  photo: state.profilePage.profile
    ? state.profilePage.profile.photos.small
    : null,
});

export default connect(mapStateToProps, { authUser, logout })(
  HeaderContainerAPI,
);
