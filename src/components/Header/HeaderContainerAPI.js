import React from 'react';
import Header from './Header';

// eslint-disable-next-line react/prefer-stateless-function
class HeaderContainerAPI extends React.Component {
  render() {
    const { login, isAuth, photo, logout } = this.props;
    return (
      <Header login={login} isAuth={isAuth} photo={photo} logout={logout} />
    );
  }
}

export default HeaderContainerAPI;
