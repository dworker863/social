import { NavLink } from 'react-router-dom';
import userPhotoDefault from 'assets/userPhotoDefault.png';
import classes from './Header.module.css';

const Header = ({ login, logout, isAuth, photo }) => {
  return (
    <header className={classes.header}>
      <img
        className={classes.logo}
        src="https://png.pngtree.com/element_pic/00/16/07/115783931601b5c.jpg"
        alt="Logo"
      />
      <div className={classes.authBlock}>
        {isAuth ? (
          <div>
            {login}
            <img
              className={classes.avatar}
              src={photo || userPhotoDefault}
              alt={login}
            />
            <button onClick={logout} type="button">
              Log Out
            </button>
          </div>
        ) : (
          <NavLink to="/login">Login</NavLink>
        )}
      </div>
    </header>
  );
};

export default Header;
