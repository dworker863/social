import { NavLink } from 'react-router-dom';
import classes from './Sidebar.module.css';

const Friend = ({ name }) => {
  return (
    <div className={classes.friend}>
      <img
        className={classes.avatar}
        src="https://image.freepik.com/free-vector/businessman-character-avatar-isolated_24877-60111.jpg"
        alt="avatar"
        width="50px"
      />
      <div className={classes.name}>{name}</div>
    </div>
  );
};

const Navbar = ({ sidebar }) => {
  return (
    <div className={classes.sidebar}>
      <nav className={classes.nav}>
        <div className={classes.item}>
          <NavLink to="/profile" activeClassName={classes.active}>
            Profile
          </NavLink>
        </div>
        <div className={classes.item}>
          <NavLink to="/dialogs" activeClassName={classes.active}>
            Messages
          </NavLink>
        </div>
        <div className={classes.item}>
          <NavLink to="/news" activeClassName={classes.active}>
            News
          </NavLink>
        </div>
        <div className={classes.item}>
          <NavLink to="/music" activeClassName={classes.active}>
            Music
          </NavLink>
        </div>
        <div className={classes.item}>
          <NavLink to="/settings" activeClassName={classes.active}>
            Settings
          </NavLink>
        </div>
        <div className={classes.item}>
          <NavLink to="/users" activeClassName={classes.active}>
            Users
          </NavLink>
        </div>
      </nav>
      <h2 className={classes.friendsTitle}>Friends</h2>
      <div className={classes.friends}>
        {sidebar.friends.map((friend) => (
          <Friend key={friend.id} name={friend.name} />
        ))}
      </div>
    </div>
  );
};

export default Navbar;
