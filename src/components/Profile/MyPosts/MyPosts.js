import { useFormik } from 'formik';
import { yupMessage } from 'utils/validate';
import * as Yup from 'yup';
import Error from 'components/common/Error/Error';
import classes from './MyPosts.module.css';
import Post from './Post/Post';

const MyPostsForm = ({ addPost }) => {
  const formik = useFormik({
    initialValues: {
      post: '',
    },
    validationSchema: Yup.object({
      post: yupMessage(),
    }),
    onSubmit(values, { resetForm }) {
      addPost(values.post);
      resetForm();
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <div>
        <textarea
          name="post"
          id="post"
          placeholder="New message"
          {...formik.getFieldProps('post')}
        />
        {formik.touched.post && formik.errors.post ? (
          <Error error={formik.errors.post} />
        ) : null}
      </div>
      <div>
        <button type="submit">Add Post</button>
      </div>
    </form>
  );
};

const MyPosts = ({ state, addPost }) => {
  return (
    <div className={classes.posts}>
      <h2>My Posts</h2>
      <div>
        <MyPostsForm addPost={addPost} />
      </div>
      <div className={classes.posts}>
        {state.posts.map((post) => (
          <Post
            key={post.id}
            message={post.text}
            likesCount={post.likesCount}
          />
        ))}
      </div>
    </div>
  );
};

export default MyPosts;
