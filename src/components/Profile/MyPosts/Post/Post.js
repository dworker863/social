import classes from './Post.module.css';

const Post = ({ message, likesCount }) => {
  return (
    <div className={classes.item}>
      <img
        src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTy1wPPtMtw7A7Mjy190HcCyZiUPKD1pNkVcm4I-s8Ymov9hJuVKIWJYoAs6bCob0Rv59U&usqp=CAU"
        alt="avatar"
        width="60px"
        height="60px"
      />
      {message}
      <div>
        <span>likes: {likesCount}</span>
      </div>
    </div>
  );
};

export default Post;
