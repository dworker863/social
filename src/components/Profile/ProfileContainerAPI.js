/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import Profile from './Profile';

class ProfileContainerAPI extends React.Component {
  componentDidMount() {
    const { getProfileStatus, getUserProfile, match } = this.props;
    const userId = match.params.userId;
    getUserProfile(userId);
    getProfileStatus(userId);
  }

  render() {
    const { state, updateProfileStatus, isAuth } = this.props;
    return (
      <div>
        <Profile
          state={state}
          updateProfileStatus={updateProfileStatus}
          isAuth={isAuth}
        />
      </div>
    );
  }
}

export default ProfileContainerAPI;
