/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prefer-stateless-function */

import withAuthRedirect from 'hoc/AuthRedirect';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import {
  getUserProfile,
  getProfileStatus,
  updateProfileStatus,
} from 'redux/profileReducer';
import ProfileContainerAPI from './ProfileContainerAPI';

const mapStateToProps = (state) => ({
  state: state.profilePage,
  isAuth: state.auth.isAuth,
});

export default compose(
  withAuthRedirect,
  connect(mapStateToProps, {
    getUserProfile,
    getProfileStatus,
    updateProfileStatus,
  }),
  withRouter,
)(ProfileContainerAPI);
