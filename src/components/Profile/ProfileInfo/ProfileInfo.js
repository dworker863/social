import Preloader from 'components/common/Preloader/Preloader';
import classes from './ProfileInfo.module.css';
import ProfileStatus from './ProfileStatus';

const ProfileInfo = ({ profile, status, updateProfileStatus }) => {
  if (!profile) return <Preloader />;

  return (
    <div>
      <div className={classes.content__bg}>
        {/* <img
          src="https://helpx.adobe.com/content/dam/help/en/photoshop/using/convert-color-image-black-white/jcr_content/main-pars/before_and_after/image-before/Landscape-Color.jpg"
          alt="BG"
        /> */}
      </div>

      <div className={classes.description}>
        <img src={profile.photos.large} alt={profile.fullname} />
        <ProfileStatus
          status={status}
          updateProfileStatus={updateProfileStatus}
        />
        <p className={classes.fullname}>Полное Имя: {profile.fullName}</p>
        <p className={classes.aboutMe}>Обо мне: {profile.aboutMe}</p>
        <p className={classes.job}>
          Работа: {profile.lookingForAJobDescription}
        </p>
      </div>
    </div>
  );
};

export default ProfileInfo;
