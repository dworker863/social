import React from 'react';

class ProfileStatus extends React.Component {
  constructor(props) {
    super(props);
    const { status } = this.props;
    this.updateStatus = this.updateStatus.bind(this);
    this.state = { editMode: false, status };
  }

  onStatusChange(e) {
    this.setState({
      status: e.currentTarget.value,
    });
  }

  updateStatus() {
    const { updateProfileStatus } = this.props;
    const { status } = this.state;

    this.setState({ editMode: false });
    updateProfileStatus(status);
  }

  render() {
    const { editMode } = this.state;
    const { status } = this.props;

    return (
      <div>
        {!editMode && (
          <div>
            <span onDoubleClick={() => this.setState({ editMode: true })}>
              {status || 'Ваш статус'}
            </span>
          </div>
        )}
        {editMode && (
          <div>
            <input
              // eslint-disable-next-line jsx-a11y/no-autofocus
              autoFocus="true"
              onChange={(e) => this.onStatusChange(e)}
              onBlur={this.updateStatus}
              type="text"
              // eslint-disable-next-line react/destructuring-assignment
              value={this.state.status}
            />
          </div>
        )}
      </div>
    );
  }
}

export default ProfileStatus;
