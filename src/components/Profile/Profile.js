import MyPostsContainer from './MyPosts/MyPostsContainer';
// import classes from './Profile.module.css';
import ProfileInfo from './ProfileInfo/ProfileInfo';

const Profile = ({ state, updateProfileStatus }) => {
  return (
    <div>
      <ProfileInfo
        profile={state.profile}
        status={state.status}
        updateProfileStatus={updateProfileStatus}
      />
      <MyPostsContainer />
    </div>
  );
};

export default Profile;
