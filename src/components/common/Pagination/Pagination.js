/* eslint-disable react/destructuring-assignment */
import { useState } from 'react';
import classes from './Pagination.module.css';

const Pagination = ({
  totalItemsCount,
  itemsCount,
  currentPage,
  onPageChange,
  portionSize = 10,
}) => {
  const pagesCount = Math.ceil(totalItemsCount / itemsCount);

  const pages = [];

  for (let i = 1; i <= pagesCount; i++) {
    pages.push(i);
  }

  const totalPortions = Math.ceil(pagesCount / portionSize);
  const [currentPortion, setCurrentPortion] = useState(1);
  const leftBorder = (currentPortion - 1) * portionSize + 1;
  const rightBorder = currentPortion * portionSize;

  return (
    <div>
      <div className={classes.pagination}>
        {currentPortion > 1 && (
          <button
            className={classes.prev}
            onClick={() => setCurrentPortion(currentPortion - 1)}
            type="button"
          >
            Prev
          </button>
        )}
        {pages
          .filter((page) => page >= leftBorder && page <= rightBorder)
          .map((page) => (
            <span
              key={page}
              className={`${classes.page} ${
                currentPage === page && classes.selected
              }`}
              onClick={() => onPageChange(page)}
              onKeyPress={() => onPageChange(page)}
              role="button"
              tabIndex={page}
            >
              {page}
            </span>
          ))}
        {currentPortion < totalPortions && (
          <button
            className={classes.next}
            onClick={() => setCurrentPortion(currentPortion + 1)}
            type="button"
          >
            Next
          </button>
        )}
      </div>
    </div>
  );
};

export default Pagination;
