import preloader from 'assets/preloader.gif';

const Preloader = () => {
  return (
    <div>
      <img src={preloader} alt="Loading" />
    </div>
  );
};

export default Preloader;
