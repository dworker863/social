import Error from 'components/common/Error/Error';
import { useFormik } from 'formik';
import { Redirect } from 'react-router-dom';
import { yupLogin, yupPassword } from 'utils/validate';
import * as Yup from 'yup';

const Login = ({ login, isAuth }) => {
  // Pass the useFormik() hook initial form values and a submit function that will
  // be called when the form is submitted
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
      rememberMe: false,
    },
    validationSchema: Yup.object({
      email: yupLogin(),
      password: yupPassword(),
    }),
    onSubmit: (values, actions) => {
      login(values.email, values.password, values.rememberMe)
        .catch((error) => {
          actions.setStatus(error.message);
        })
        .finally(() => {
          actions.setSubmitting(false);
        });
    },
  });

  if (isAuth) return <Redirect to="/profile" />;

  return (
    <form onSubmit={formik.handleSubmit}>
      <div>
        <label htmlFor="email">Email: </label>
        <input
          id="email"
          name="email"
          type="text"
          {...formik.getFieldProps('email')}
        />
        {formik.touched.email && formik.errors.email ? (
          <Error error={formik.errors.email} />
        ) : null}
      </div>
      <div>
        <label htmlFor="email">Password: </label>
        <input
          id="password"
          name="password"
          type="password"
          {...formik.getFieldProps('password')}
        />
        {formik.touched.password && formik.errors.password ? (
          <Error error={formik.errors.password} />
        ) : null}
      </div>
      <div>
        <label htmlFor="rememberMe">remember: </label>
        <input
          id="rememberMe"
          name="rememberMe"
          type="checkbox"
          {...formik.getFieldProps('rememberMe')}
        />
        {formik.touched.rememberMe && formik.errors.rememberMe ? (
          <Error error={formik.errors.rememberMe} />
        ) : null}
      </div>
      <button type="submit">Login</button>
      {formik.status && <Error error={formik.status} />}
    </form>
  );
};

export default Login;
